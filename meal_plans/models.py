from django.conf import settings
from django.db import models

# Create your models here.

USER_MODEL = settings.AUTH_USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=100)
    owner = models.ForeignKey(
        USER_MODEL, related_name="meal_plans", on_delete=models.CASCADE
    )
    date = models.DateTimeField()

    recipes = models.ManyToManyField(
        "recipes.Recipe", related_name="meal_plans"
    )
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
