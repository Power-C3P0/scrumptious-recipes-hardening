from django import forms

from recipes.models import Rating, ShoppingItem


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]


class Shoppingitemform(forms.ModelForm):
    model = ShoppingItem
