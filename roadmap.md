## requirements roadmap
user:
    userone , pw: hackreactor   
    usertwo , pw: hackreactor

    
------------mealplans needs to be its own django APP named meal_plans
    create meal_plans app

--------create MealPlan model
    include:
    name: string
    date: date
    owner: AUTH_USER_MODEL :foreign key
    recipes: recipes.Recipe
    update admin page with new models

meal plan paths
       see document for paths

----------create ListView
    1 make list.html
    2 link to "meal_plan/new.html
    3 only meal plans made ny user shown
    4 each plan has link to it's detail page 
    5 context_object_name....put that shit in the list view class.

create CreateView
(NEED INFO ON THE save_m2m)

create DetailView(meal_plans/<int:pk>/)
    1 make detail.html file
    2 extend from base.html
    3 add tags to detail.html 
    4 figure out the user access view(if user is not owner, should go to create meal plan page/ yell at them page) could be done on the page but maybe in the views

create EditView(meal_plans/<int:pk>/edit/)
    1 make edit.html
    2 extend from base.html
    3 add form tags
    4 owner access only
    5 figure out the user access view(if user is not owner, should go to create meal plan page/ yell at them page) could be done on the page but maybe in the views


create DeleteView
    1 make delete.html
    2 add form tag
    3 only owner can exec delete func
    3 redirect back to meal_plan_list
    4 figure out the user access view(if user is not owner, should go to create meal plan page/ yell at them page) could be done on the page but maybe in the views






            [[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[           SHOPPING LIST               ]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]

Users only need a single shopping list.

The MVP for this project is four features:

1. A "+ shopping list" button next to any ingredient not already in the shopping list that, when clicked, adds it to the shopping list

2. A main nav link that links to a list of the current items in the shopping list with an indicator of how many items are in the shopping list

3. The actual shopping list of food items that have been added

4. A button on the list page to clear all items from the shopping list



- the shopping list link at the top with a count of how many items are in the shopping list for that user
- each user has their own shopping list
- a button and view to clear the shopping list
- a button and view to add an ingredient's food item to the shopping list

TWO VIEWS ARE FUNC REQUIRED

        the view to clear the shopping list by deleting all of the shopping items; the DeleteView is really only good for deleting a single item

        the view to add an ingredient's food item to the shopping list because when we show it, it's not a normal form



REQUIREMENTS

The following sections describe the technical requirements for the different aspects of the system.

Code location:
The shopping list feature should be in the recipes app.

The ShoppingItem model:
Each shopping item should have two pieces of data on it.

        user	AUTH_USER_MODEL	foreign key to the Django user model, cascade on delete

        food_item	"FoodItem"	foreign key to FoodItem, protect on delete

    Each association of a person and a food item should be unique


    It means that a user can only add a food item once to their shopping list. That way, the person doesn't have 13 duplicate "onion" items in the list. (That's a lot of onions.)

    One user can have many shopping items. One shopping item can have one user. This is a one-to-many relationship with the many side being the shopping items. You will need a foreign key from the shopping item model to the user model.

    Refer to your Recipe model to see how to create a foreign key to the AUTH_USER_MODEL.

    One shopping item can have one food item. One food item can be on many different users' shopping lists. That means it's a one-to-many relationship from food item to shopping item. You will need a foreign key.

    Refer to your Ingredient model to see how to create a foreign key relationship to FoodItem.


